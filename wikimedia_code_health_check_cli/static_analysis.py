#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json
import os
import subprocess
import wikimedia_code_health_check_cli.util as util


def get_sast_config():
    """TODO: these policies aren't OSI-compliant, sadly"""
    return {
        "semgrep": [
            "scan",
            "--json",
            "--metrics=off",
            "--exclude=vendor",
            "--exclude=node_modules",
            "--config=p/security-audit",
            "--config=p/ci",
            "--config=p/cwe-top-25",
            "--config=p/secrets",
            "--config=p/supply-chain",
            "--config=p/insecure-transport",
            "--config=p/owasp-top-ten",
            "--config=p/r2c-ci",
            "--config=p/jwt",
            "--config=p/xss",
            "--config=p/javascript",
            "--config=p/typescript",
            "--config=p/nodejsscan",
            "--config=p/nodejs",
            "--config=p/insecure-transport-jsnode",
            "--config=p/clientside-js",
            "--config=r/php",
            "--config=p/phpcs-security-audit",
            "--config=p/react",
            "--config=p/nextjs",
            "--config=p/gitleaks",
            "--config=p/lockfiles",
            "--config=p/expressjs",
            "--config=p/sql-injection",
            "--config=p/command-injection",
            "--config=p/security-code-scan",
        ]
    }


def get_sast_scores_config():
    return {
        "20": 10,
        "10": 5,
        "5": 2,
    }


def check_sast_dependencies(lockfile_config={}):
    checked = {}
    for dep, subdeps in lockfile_config.items():
        if dep not in checked.keys():
            if util.check_if_dependency_exists(dep):
                checked[dep] = True
            else:
                checked[dep] = False
    return checked


def get_package_file_paths_to_scan(git_repo_path=""):
    return [git_repo_path]  # just do everything for now


def attempt_sast_scan(git_repo_path="."):
    sast_config = get_sast_config()
    scores_config = get_sast_scores_config()
    found_vulns = []
    found_vulns_count = 0
    total_score = 0

    """run SAST tool(s), parse results"""
    checked_deps = check_sast_dependencies(sast_config)
    checked_deps_valid = False if False in checked_deps.values() else True

    if checked_deps_valid:
        base_dir = os.getcwd()
        for p in get_package_file_paths_to_scan(git_repo_path):
            if os.path.exists(p):
                os.chdir(p)
                sast_tool_out = None
                try:
                    sast_cmd = [list(sast_config.keys())[0]] + list(
                        sast_config.values()
                    )[0]
                    sast_tool_out = subprocess.run(
                        sast_cmd,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.DEVNULL,
                    )
                except subprocess.CalledProcessError as err:
                    print(str(err))
                finally:
                    sast_results = json.loads(sast_tool_out.stdout)
                    found_vulns = sast_results["results"]
                    found_vulns_count = len(found_vulns)
        os.chdir(base_dir)
    else:
        print("Unable to run sast scans due to missing dependencies:")
        for dep in checked_deps:
            if checked_deps[dep] is False:
                print(f"\n* {dep}\n")

    for s, v in scores_config.items():
        if found_vulns_count > int(s):
            total_score += int(v)
            break

    return total_score
