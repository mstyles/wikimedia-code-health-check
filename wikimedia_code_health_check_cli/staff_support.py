#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import csv
import datetime
import git
import json
import os


def get_staff_support_config():
    return {
        "time-period": (2 * 365),
        "pkgfiles": {
            "extension.json": ["author"],
            "pacakge.json": ["author", "maintainers", "contributors"],
            "composer.json": ["authors"],
        },
        "wmf_staff_file": "includes/wmf_staff.csv",
        "scores": {
            "15": 10,
            "20": 5,
            "25": 2,
        },
    }


def get_wmf_staff_list():
    c = get_staff_support_config()
    csv_data = []
    fn = c["wmf_staff_file"]
    if os.path.exists(fn):
        with open(fn, "r") as f:
            reader = csv.reader(f)
            for row in reader:
                csv_data.append(row)
    return csv_data


def get_package_file_authors(git_repo_path):
    raw_author_data = []
    c = get_staff_support_config()
    for pkgfile, keys in c["pkgfiles"].items():
        fn = git_repo_path + "/" + pkgfile
        if os.path.exists(fn):
            with open(fn, "rb") as f:
                json_data = json.load(f)
            for k in keys:
                if k in json_data:
                    raw_author_data = json_data[k]
    return raw_author_data


def get_git_authors(git_repo_path, branch):
    c = get_staff_support_config()
    g = git.Git(git_repo_path)
    since = datetime.date.today() - datetime.timedelta(days=c["time-period"])
    uniq_contribs = []
    users = {}

    base_dir = os.getcwd()
    os.chdir(git_repo_path)
    loginfo = g.log(
        f"--since={since}",
        "--pretty=format:%an | %ae",
        "--no-patch",
        "--perl-regexp",
        r"--author=^((?!libraryupgrader|Translation\ updater\ bot|jenkins\-bot|Gerrit\ maintenance\ bot|gerritbot).*)$",  # noqa E501
        branch,
    )
    os.chdir(base_dir)
    uniq_contribs = set(loginfo.split("\n"))
    for data in uniq_contribs:
        u = data.split(" | ")
        users[u[0]] = u[1]

    return users


def get_staff_support_score(git_repo_path=".", branch="master"):
    c = get_staff_support_config()
    total_score = 0

    wmf_staff = get_wmf_staff_list()
    pkgfile_authors = get_package_file_authors(git_repo_path)
    git_authors = get_git_authors(git_repo_path, branch)

    merged_users = set(
        [item for sublist in wmf_staff for item in sublist]
        + [item for sublist in pkgfile_authors for item in sublist]
    )

    likely_wmf_staff_found = []
    for un, ue in git_authors.items():
        if un in merged_users:
            likely_wmf_staff_found.append(un)
        elif ue in merged_users:
            likely_wmf_staff_found.append(ue)

    for s, v in c["scores"].items():
        if len(likely_wmf_staff_found) < int(s):
            total_score += int(v)
            break

    return total_score
