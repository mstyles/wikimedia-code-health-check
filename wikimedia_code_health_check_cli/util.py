#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import subprocess
from shlex import quote as shlex_quote


def check_if_dependency_exists(cmd):
    """simple check of fs for bin/script"""
    return_val = False
    valid_cmd = subprocess.run(
        ["type", "-a", shlex_quote(cmd)],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
    )
    if valid_cmd.returncode == 0:
        return_val = True
    return return_val
