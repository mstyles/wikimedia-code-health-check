#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json
import os


def get_lang_guidelines_config():
    return {
        "php": {
            "pkgfile": "composer.json",
            "dep_keys": {
                "require-dev": [
                    "mediawiki/mediawiki-codesniffer",
                    "mediawiki/mediawiki-phan-config",
                    "mediawiki/minus-x",
                    "php-parallel-lint/php-parallel-lint",
                ],
                "scripts": ["test", "phpcs", "phan"],
            },
        },
        "node": {
            "pkgfile": "package.json",
            "dep_keys": {
                "scripts": ["api-testing", "doc", "test", "coverage"],
                "devDependencies": [
                    "api-testing",
                    "eslint-config-wikimedia",
                    "grunt",
                    "grunt-banana-checker",
                    "grunt-eslint",
                    "grunt-stylelint",
                    "stylelint-config-wikimedia",
                ],
            },
        },
    }


def get_lang_guidelines_scores_config():
    return {
        "valid_dep_keys": {"15": 10},
    }


def check_pkgfiles_exist(git_repo_path, config):
    std_pkgfiles_exist = False
    std_pkgfiles = []

    for lang, info in config.items():
        file_path = git_repo_path + "/" + info["pkgfile"]
        if os.path.exists(file_path):
            std_pkgfiles.append(True)
        else:
            std_pkgfiles.append(False)

    if False not in std_pkgfiles:
        std_pkgfiles_exist = True
    return std_pkgfiles_exist


def check_for_valid_dep_keys(git_repo_path, config):
    valid_dep_keys = 0

    for lang, info in config.items():
        pkgfile_data = {}
        pkgfile_path = git_repo_path + "/" + info["pkgfile"]
        if os.path.exists(pkgfile_path):
            pkgfile_data = open(pkgfile_path, "r").read()
            pkgfile_data = json.loads(pkgfile_data)
        for key, subkeys in info["dep_keys"].items():
            for subkey in subkeys:
                if pkgfile_data.get(key, {}).get(subkey):
                    valid_dep_keys += 1

    return valid_dep_keys


def calculate_lang_guidelines_score(git_repo_path="."):
    c = get_lang_guidelines_config()
    s = get_lang_guidelines_scores_config()
    total_score = 0

    if check_pkgfiles_exist(git_repo_path, c):
        dep_count = check_for_valid_dep_keys(git_repo_path, c)
        for k, v in s["valid_dep_keys"].items():
            if dep_count > int(v):
                total_score += int(v)

    return total_score
