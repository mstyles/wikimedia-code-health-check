#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import datetime
import git
import os


def get_non_auto_commits_config():
    return {
        "time-period": (2 * 365),
        "scores": {
            "30": 10,
            "150": 5,
            "300": 2,
        },
    }


def get_non_auto_commits(git_repo_path=".", branch="master"):
    c = get_non_auto_commits_config()
    g = git.Git(git_repo_path)
    since = datetime.date.today() - datetime.timedelta(days=c["time-period"])
    total_score = 0

    base_dir = os.getcwd()
    os.chdir(git_repo_path)
    loginfo = g.log(
        f"--since={since}",
        '--pretty=format:"%h"',
        "--no-patch",
        "--perl-regexp",
        r"--author=^((?!libraryupgrader|Translation\ updater\ bot|jenkins\-bot|Gerrit\ maintenance\ bot|gerritbot).*)$",  # noqa E501
        branch,
    )
    os.chdir(base_dir)

    commit_count = len(loginfo.split("\n"))
    for s, v in c["scores"].items():
        if commit_count < int(s):
            total_score += int(v)
            break

    return total_score
