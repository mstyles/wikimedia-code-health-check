#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import texttable


def calculate_weighted_risk_score(results):
    risk_weights = {
        "vulnerable_packages": 0.8,
        "package_management": 0.8,
        "test_coverage": 0.6,
        "sast": 0.5,
        "non_auto_repo_commits": 0.6,
        "unique_contributors": 0.4,
        "contributor_concentration": 1,
        "language_guidelines": 0.3,
        "staff_support": 1,
        "task_backlog": 0.7,
        "code_stewardship": 1,
    }
    total_risk = 0.0
    for c in risk_weights:
        if c in results:
            total_risk += float(risk_weights[c]) * float(results[c])
    return "%.2f" % total_risk


def output_report_results(
    search, project_git_branch, project_repo_url, results, mode="table"
):
    """calculate weighted risk total here"""
    results["weighted_risk"] = calculate_weighted_risk_score(results)

    if mode == "table":
        """print in various output formats - only tabular for now"""
        print(f"PROJECT: {search}@{project_git_branch}")
        print(f"REPOSITORY: {project_repo_url}\n")

        col_defaults = {
            "col_align": "r",
            "col_valign": "t",
            "col_dtype": "t",
        }

        col_name_data = {
            "vulnerable_packages": ["Vuln Pkgs", 9],
            "package_management": ["Pkg Mgmt", 8],
            "test_coverage": ["Test Cov", 8],
            "sast": ["SAST", 4],
            "non_auto_repo_commits": ["Non-auto Cmts", 13],
            "unique_contributors": ["Uniq Contribs", 13],
            "contributor_concentration": ["Contrib Conc", 12],
            "language_guidelines": ["Lang Guides", 11],
            "staff_support": ["Staff Supp", 10],
            "task_backlog": ["Task Backlog", 12],
            "code_stewardship": ["Code Stew", 9],
            "weighted_risk": ["Weighted Risk", 13],
        }

        table = texttable.Texttable()
        col_count = 0
        for cnd in col_name_data:
            if cnd in results:
                col_count += 1

        table.set_cols_align([col_defaults["col_align"]] * col_count)
        table.set_cols_valign([col_defaults["col_valign"]] * col_count)
        table.set_cols_dtype([col_defaults["col_dtype"]] * col_count)

        col_widths = []
        col_hdr_names = []
        col_data = []
        for cnd in results:
            col_widths.append(col_name_data[cnd][1])
            col_hdr_names.append(col_name_data[cnd][0])
            col_data.append(results[cnd])
        table.set_cols_width(col_widths)
        table.add_rows([col_hdr_names, col_data])

        print(table.draw())
