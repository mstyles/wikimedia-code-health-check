#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json
import os
import subprocess
import wikimedia_code_health_check_cli.util as util
from pathlib import Path


def get_lockfile_config():
    return {
        "php": {
            "pkgfile": "composer.json",
            "lockfile": "composer.lock",
            "install_cmd": "composer.phar install",
            "sca_tool_cmd": "osv-scanner --lockfile=composer.lock --json",
        },
        "node": {
            "pkgfile": "package.json",
            "lockfile": "package-lock.json",
            "install_cmd": "npm i --save-dev --package-lock-only",
            "sca_tool_cmd": "osv-scanner --lockfile=package-lock.json --json",
        },
    }


def get_vuln_pkgs_scores_config():
    return {
        "15": 10,
        "10": 5,
        "3": 2,
    }


def check_lockfile_dependencies(lockfile_config={}):
    checked = {}
    for dep, subdeps in lockfile_config.items():
        if dep not in checked.keys():
            if util.check_if_dependency_exists(dep):
                checked[dep] = True
            else:
                checked[dep] = False
        if len(subdeps) > 0:
            for i, subdepkey in enumerate(subdeps):
                if subdepkey in ["install_cmd", "sca_tool_cmd"]:
                    subdep_tool = subdeps[subdepkey].split(" ", 1)[0]
                    if subdep_tool not in checked.keys():
                        if util.check_if_dependency_exists(subdep_tool):
                            checked[subdep_tool] = True
                        else:
                            checked[subdep_tool] = False
    return checked


def get_package_file_paths(lockfile_config={}, git_repo_path=""):
    package_file_paths = {}
    for lang, config in lockfile_config.items():
        for path in Path(git_repo_path).rglob(config["pkgfile"]):
            package_file_paths[path.as_posix()] = lang
    return package_file_paths


def attempt_lockfile_scans(git_repo_path="."):
    lockfile_config = get_lockfile_config()
    scores_config = get_vuln_pkgs_scores_config()
    total_vulns = {}
    total_vulns["total"] = 0
    total_score = 0

    """iterate through package/lock files, run SCA tooling, parse results"""
    checked_deps = check_lockfile_dependencies(lockfile_config)
    checked_deps_valid = False if False in checked_deps.values() else True

    if checked_deps_valid:
        package_file_paths = get_package_file_paths(lockfile_config, git_repo_path)

        base_dir = os.getcwd()
        for package_file_path, lang in package_file_paths.items():
            p = Path(package_file_path)
            package_dir = p.parent.as_posix()
            os.chdir(package_dir)
            lockfile_cmd = lockfile_config[lang]["install_cmd"]
            sca_tool_cmd = lockfile_config[lang]["sca_tool_cmd"]

            lockfile_cmd_out = None
            lockfile_cmd_out = subprocess.run(
                lockfile_cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.DEVNULL
            )

            sca_tool_out = None
            if lockfile_cmd_out.returncode == 0:
                try:
                    sca_tool_out = subprocess.run(
                        sca_tool_cmd.split(),
                        stdout=subprocess.PIPE,
                        stderr=subprocess.DEVNULL,
                    )
                except subprocess.CalledProcessError as err:
                    if err.returncode > 0:  # osv-scanner returns 1 for results
                        pass
                finally:
                    sca_results = json.loads(sca_tool_out.stdout)
                    for p in sca_results["results"]:
                        found_vulns_count = len(p["packages"])
                        total_vulns["total"] = found_vulns_count
                        total_vulns[package_file_path] = found_vulns_count
            os.chdir(base_dir)
    else:
        print("Unable to run lockfile scans due to missing dependencies:")
        for dep in checked_deps:
            if checked_deps[dep] is False:
                print(f"\n* {dep}\n")

    for s, v in scores_config.items():
        if total_vulns["total"] > int(s):
            total_score += int(v)
            break

    return total_score
