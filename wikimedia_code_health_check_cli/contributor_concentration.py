#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import datetime
import git
import numpy
import os


def get_contrib_conc_config():
    return {
        "time_period": (2 * 365),
        "low_commit_rate": 3,
        "scores": {
            "low_commit_score": {"25": 10, "50": 5, "200": 2},
            "low_commit_rate_score": {"75": 10, "50": 5, "25": 2},
            "high_std_dev_score": {"10": 0, "50": 5, "100": 10},
        },
    }


def get_contrib_conc(git_repo_path=".", branch="master"):
    c = get_contrib_conc_config()
    g = git.Git(git_repo_path)
    since = datetime.date.today() - datetime.timedelta(days=c["time_period"])
    total_score = 0

    base_dir = os.getcwd()
    os.chdir(git_repo_path)
    loginfo = g.log(
        f"--since={since}",
        '--pretty=format:"%an"',
        "--no-patch",
        "--perl-regexp",
        r"--author=^((?!libraryupgrader|Translation\ updater\ bot|jenkins\-bot|Gerrit\ maintenance\ bot|gerritbot).*)$",  # noqa E501
        branch,
    )
    os.chdir(base_dir)

    user_contribs = []
    unique_contributors = set(loginfo.split("\n"))
    for u in unique_contributors:
        user = u.replace('"', "").replace(" ", r"\ ")
        unique_loginfo = g.log(
            f"--since={since}",
            '--pretty=format:"%h"',
            "--no-patch",
            "--author=" + user,
            branch,
        )
        user_contribs.append(len(unique_loginfo.split("\n")))

    os.chdir(base_dir)

    num_commits = len(loginfo.split("\n"))
    for s, v in c["scores"]["low_commit_score"].items():
        if num_commits < int(s):
            total_score += int(v)
            break

    low_num_commit_percent = 0
    low_commits = []
    for u in user_contribs:
        if u < c["low_commit_rate"]:
            low_commits.append(u)
    low_num_commit_percent = round(len(low_commits) / len(user_contribs), 2) * 100
    for s, v in c["scores"]["low_commit_rate_score"].items():
        if low_num_commit_percent > int(s):
            total_score += int(v)
            break

    std_dev = numpy.std(user_contribs)
    for s, v in c["scores"]["high_std_dev_score"].items():
        if std_dev < int(s):
            total_score += int(v)
            break

    return total_score
