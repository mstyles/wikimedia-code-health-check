#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import git
import os
import pytest
from unittest.mock import patch
from wikimedia_code_health_check_cli.git_utils import (
    process_project_with_branch_or_tag,
    attempt_local_clone_and_checkout,
)


def test_process_project_with_branch_or_tag():
    """test basic str processing"""
    test_search_no_branch = "Examples"
    test_search_branch = "Examples@REL1_40"
    assert process_project_with_branch_or_tag(test_search_no_branch) == (
        "Examples",
        "master",
    )
    assert process_project_with_branch_or_tag(test_search_branch) == (
        "Examples",
        "REL1_40",
    )


@patch.dict(os.environ, {"GIT_CLONE_PATH": "."})
def test_attempt_local_clone_and_checkout_bad_url():
    project_name = "Examples"
    project_repo_url = "A_bad_url"
    project_git_branch = "REL1_40"

    with pytest.raises(RuntimeError):
        attempt_local_clone_and_checkout(
            project_name, project_repo_url, project_git_branch
        )


@patch.dict(os.environ, {"GIT_CLONE_PATH": "."})
@patch("shutil.rmtree")
@patch("git.Repo")
@patch("git.Git")
def test_attempt_local_clone_and_checkout_bad_git_cmd(
    mock_rmtree, mock_git_repo, mock_git
):
    project_name = "Examples"
    project_repo_url = (
        "https://gerrit.wikimedia.org/r/mediawiki/extensions/examples"  # noqa E501
    )
    project_git_branch = "REL1_40"

    mock_rmtree.return_value = True
    p = mock_git.return_value = False
    type(mock_git_repo.clone_from.return_value).bare = p
    mock_git_repo.clone_from.return_value.git.checkout.side_effect = git.exc.GitError(
        "Something bad happened..."
    )  # noqa E501

    assert (
        len(
            attempt_local_clone_and_checkout(
                project_name, project_repo_url, project_git_branch
            )
        )
        > 0
    )


@patch.dict(os.environ, {"GIT_CLONE_PATH": "."})
@patch("shutil.rmtree")
@patch("git.Repo")
@patch("git.Git")
def test_attempt_local_clone_and_checkout_success(
    mock_rmtree, mock_git_repo, mock_git
):  # noqa E501
    project_name = "Examples"
    project_repo_url = (
        "https://gerrit.wikimedia.org/r/mediawiki/extensions/examples"  # noqa E501
    )
    project_git_branch = "REL1_40"

    mock_rmtree.return_value = True
    p = mock_git.return_value = False
    type(mock_git_repo.clone_from.return_value).bare = p
    mock_git_repo.clone_from.return_value.git.checkout.return_value = True

    assert (
        attempt_local_clone_and_checkout(
            project_name, project_repo_url, project_git_branch
        )
        is True
    )
