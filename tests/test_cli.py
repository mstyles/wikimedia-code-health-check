#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import re
from click.testing import CliRunner
from wikimedia_code_health_check_cli.cli import cli


def test_cli():
    runner = CliRunner()

    """test help message"""
    result = runner.invoke(cli, ["--help"])
    assert result.exit_code == 0
    re_test_1 = re.compile(
        r"Usage\:(\s{1,32})(\w{1,32})(\s{1,32})\[OPTIONS\](\s{1,32})SEARCH"
    )
    assert re_test_1.search(result.output)
